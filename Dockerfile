FROM alpine:latest
RUN mkdir lazy
WORKDIR lazy
COPY requirements.txt /lazy/
COPY setup_cronjob.sh /lazy/
COPY holidays_crawler.py /lazy/ 
COPY login.sh /lazy/
COPY logout.sh /lazy/
COPY wtf.py /lazy/
COPY config /lazy/


RUN echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories &&\
    echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories &&\
    apk update && apk upgrade &&\
    apk add --no-cache chromium@edge nss@edge &&\
    apk add --no-cache python3 python3-dev py-pip curl unzip libexif udev chromium chromium-chromedriver xvfb &&\
    apk --no-cache \
        add musl \
        musl-dev \
        linux-headers \
        gcc \
        g++ \
        make \
        gfortran \
        openblas-dev &&\
    python3 -m ensurepip &&\
    rm -r /usr/lib/python*/ensurepip &&\
    pip3 install --upgrade pip setuptools &&\
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi &&\
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi &&\
    rm -r /root/.cache &&\
    pip3 install --requirement /lazy/requirements.txt &&\
    rm -rf /var/lib/apt/lists/* /var/cache/apk/* /usr/share/man /tmp/* &&\
    /lazy/setup_cronjob.sh &&\
    python3 holidays_crawler.py 
