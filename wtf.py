from selenium import webdriver
from pyvirtualdisplay import Display
import time
from numpy import random
from datetime import date
import sys
import configparser
import argparse

today = str(date.today())
with open('ignore','r') as f:
    ignores = [d.strip() for d in f.readlines()]
for d in ignores:
    if d == today:
        sys.exit()

config = configparser.ConfigParser()
config.read('config')
USER = config['ID']['USER']
PASSWD = config['ID']['PASSWD']

parser = argparse.ArgumentParser()
parser.add_argument("-t", "--type", help="type include:[login|logout]")
args = parser.parse_args()
wtf_type = args.type
if wtf_type not in ['login','logout']:
    print('please type in correct type, use "python wtf.py --help" to check the rule')
    sys.exit()

sleep_minute = int(config['SLEEP'][wtf_type])
sleep_time = random.rand()*sleep_minute*59
time.sleep(sleep_time)

display=Display(visible=0,size=(800,800))
display.start()

url = "https://my.ntu.edu.tw/attend/ssi.aspx"
try:
    driver = webdriver.Chrome('chromedriver')
except:
    from selenium.webdriver.chrome.options import Options
    chrome_options = Options()
    chrome_options.add_argument("--no-sandbox")
    driver = webdriver.Chrome(chrome_options=chrome_options)
driver.get(url)
driver.find_element_by_css_selector('#divLogin a.btn').click()
time.sleep(2)
driver.find_element_by_css_selector("#myTable td input").send_keys(USER)
time.sleep(2)
driver.find_element_by_css_selector("#myTable2 td input").send_keys(PASSWD)
time.sleep(2)
driver.find_element_by_css_selector("#myTable2 +tr input").click()
time.sleep(2)
if wtf_type == 'login':
    driver.find_element_by_css_selector("#btSign").click()
elif wtf_type == 'logout':
    driver.find_element_by_css_selector("#btSign2").click()
driver.quit()

display.stop()
