import requests as req
import re
import sys
from datetime import date

def clean_date(d):
    year,month,day = d["date"].split("/")
    return ("%04d-%02d-%02d"%(int(year),int(month),int(day)))

url = "http://data.ntpc.gov.tw/api/v1/rest/datastore/382000000A-000077-002"
res = req.get(url).json()
if not res["success"]: sys.exit()

today = date.today().strftime("%Y/%m/%d")
data = res["result"]["records"]
recs = list(filter(lambda x:x["isHoliday"]=='是', data))
recs = list(filter(lambda x:x["date"]>=today, recs))
recs = list(map(lambda x:clean_date(x),recs))

with open("ignore","w") as f:
    f.write("\n".join(recs)) 
